import React, { Component } from 'react';
import {
    View, 
    Text,
    StyleSheet,
    Button
} from 'react-native';
import {Card, ListItem} from 'react-native-elements';

export default class Home extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Card
                    title='Hello World'
                >
                <Text style={{margin: 10}}></Text>
                <Button
                    title='View More'
                    onPress={()=> '#'}
                />
                </Card>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})